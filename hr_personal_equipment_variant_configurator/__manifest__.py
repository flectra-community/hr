# Copyright 2021 Creu Blanca
# License AGPL-3.0 or later (http://www.gnu.org/licenses/agpl).

{
    "name": "Hr Personal Equipment Variant Configurator",
    "summary": """
        Manage variants of personal equipment""",
    "version": "2.0.1.0.0",
    "license": "AGPL-3",
    "author": "CreuBlanca, Odoo Community Association (OCA)",
    "website": "https://gitlab.com/flectra-community/hr",
    "depends": ["hr_personal_equipment_request", "product_variant_configurator"],
    "data": ["views/hr_personal_equipment_request.xml"],
}
