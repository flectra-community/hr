# Copyright 2017 Eficent Business and IT Consulting Services S.L.
# License AGPL-3.0 or later (https://www.gnu.org/licenses/agpl).
{
    "name": "HR Holidays Settings",
    "summary": "Enables Settings Form for HR Holidays.",
    "version": "2.0.1.0.0",
    "category": "Human Resources",
    "website": "https://gitlab.com/flectra-community/hr",
    "author": "ForgeFlow, Odoo Community Association (OCA)",
    "license": "AGPL-3",
    "depends": ["hr_holidays"],
    "data": ["views/res_config_settings.xml"],
    "installable": True,
}
