# License AGPL-3.0 or later (https://www.gnu.org/licenses/agpl).
{
    "name": "HR Branch",
    "summary": "Allow define company branch for employee process",
    "version": "2.0.1.0.0",
    "development_status": "Mature",
    "category": "Human Resources",
    "website": "https://gitlab.com/flectra-community/hr",
    "author": "Vauxoo, Odoo Community Association (OCA)",
    "maintainers": ["luistorresm"],
    "license": "LGPL-3",
    "application": False,
    "installable": True,
    "depends": ["hr"],
    "data": ["views/hr_department_views.xml", "views/hr_employee_views.xml"],
    "demo": [],
    "qweb": [],
}
