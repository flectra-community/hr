# Flectra Community / hr

None



Available addons
----------------

addon | version | summary
--- | --- | ---
[hr_contract_employee_calendar_planning](hr_contract_employee_calendar_planning/) | 2.0.1.0.0| Hr Contract Employee Calendar Planning
[hr_course_survey](hr_course_survey/) | 2.0.1.0.1|         Evaluate a course using a Schedule
[hr_job_category](hr_job_category/) | 2.0.1.0.0| Adds tags to employee through contract and job position
[hr_worked_days_from_timesheet](hr_worked_days_from_timesheet/) | 2.0.1.0.0| Adds a button to import worked days from timesheet.
[hr_employee_phone_extension](hr_employee_phone_extension/) | 2.0.1.0.0| Employee Phone Extension
[hr_employee_firstname](hr_employee_firstname/) | 2.0.3.0.0| Adds First Name to Employee
[hr_employee_digitized_signature](hr_employee_digitized_signature/) | 2.0.1.0.0| Employee Digitized Signature
[hr_employee_partner_external](hr_employee_partner_external/) | 2.0.1.0.0| Associate an external Partner to Employee
[hr_employee_id](hr_employee_id/) | 2.0.1.0.0| Employee ID
[hr_employee_relative](hr_employee_relative/) | 2.0.1.0.0| Allows storing information about employee's family
[hr_contract_multi_job](hr_contract_multi_job/) | 2.0.1.0.0| HR Contract Multi Jobs
[hr_recruitment_notification](hr_recruitment_notification/) | 2.0.1.0.0| Configure automatic notifications for new applications
[hr_employee_service](hr_employee_service/) | 2.0.1.1.1| Employee service information & duration
[hr_department_code](hr_department_code/) | 2.0.1.0.0| HR department code
[hr_employee_document](hr_employee_document/) | 2.0.1.0.0| Documents attached to the employee profile
[hr_employee_age](hr_employee_age/) | 2.0.1.0.0| Age field for employee
[hr_employee_ppe](hr_employee_ppe/) | 2.0.1.0.0| Personal Protective Equipment (PPE) Management
[hr_contract_currency](hr_contract_currency/) | 2.0.1.0.0| Employee's contract currency
[hr_employee_service_contract](hr_employee_service_contract/) | 2.0.1.0.0| Employee service information & duration based on employee's contracts
[hr_org_chart_overview](hr_org_chart_overview/) | 2.0.1.0.0| Organizational Chart Overview
[hr_personal_equipment_stock](hr_personal_equipment_stock/) | 2.0.1.0.0|         This addon allows to integrate hr_personal_equipment_request with stock
[hr_employee_lastnames](hr_employee_lastnames/) | 2.0.3.0.0| Split Name in First Name, Father's Last Name and Mother's Last Name
[hr_holidays_settings](hr_holidays_settings/) | 2.0.1.0.0| Enables Settings Form for HR Holidays.
[hr_branch](hr_branch/) | 2.0.1.0.0| Allow define company branch for employee process
[hr_employee_ssn](hr_employee_ssn/) | 2.0.1.0.0| View/edit employee's SSN & SIN fields
[hr_personal_equipment_request_tier_validation](hr_personal_equipment_request_tier_validation/) | 2.0.1.0.0|         Enables tier validation from hr.personal.equipment.request
[resource_hook](resource_hook/) | 2.0.1.0.0|         Extends the resource with hooks to standard methods.
[hr_employee_medical_examination](hr_employee_medical_examination/) | 2.0.1.0.1|         Adds information about employee's medical examinations
[hr_employee_birth_name](hr_employee_birth_name/) | 2.0.1.0.1| Employee Birth Name
[hr_contract_reference](hr_contract_reference/) | 2.0.1.0.0| HR Contract Reference
[hr_contract_rate](hr_contract_rate/) | 2.0.1.0.0| Employee's contract rate and period
[gamification_badge_report](gamification_badge_report/) | 2.0.1.0.0| Send Email to all the Employees with all the award given in     the last week.
[hr_contract_document](hr_contract_document/) | 2.0.1.0.1| Documents attached to the contact
[hr_course](hr_course/) | 2.0.3.0.3|         This module allows your to manage employee's training courses
[hr_contract_type](hr_contract_type/) | 2.0.1.0.0|         Add a Type for Contracts
[hr_personal_equipment_variant_configurator](hr_personal_equipment_variant_configurator/) | 2.0.1.0.0|         Manage variants of personal equipment
[hr_employee_calendar_planning](hr_employee_calendar_planning/) | 2.0.1.5.2| Employee Calendar Planning
[hr_personal_equipment_request](hr_personal_equipment_request/) | 2.0.1.0.0|         This addon allows to manage employee personal equipment
[hr_period](hr_period/) | 2.0.1.0.0| Add payroll periods


