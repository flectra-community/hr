# Copyright 2020 Escflectra
# License AGPL-3.0 or later (http://www.gnu.org/licenses/agpl).

{
    "name": "Personal Protective Equipment (PPE) Management",
    "version": "2.0.1.0.0",
    "author": "Escflectra, Odoo Community Association (OCA)",
    "maintainers": ["marcelsavegnago", "eduaparicio"],
    "images": ["static/description/banner.png"],
    "website": "https://gitlab.com/flectra-community/hr",
    "license": "AGPL-3",
    "category": "Human Resources",
    "depends": ["hr_personal_equipment_request"],
    "data": [
        "views/product_template.xml",
        "views/hr_personal_equipment.xml",
        "views/hr_personal_equipment_request.xml",
        "data/hr_employee_ppe_cron.xml",
        "reports/hr_employee_ppe_report_template.xml",
        "reports/hr_employee_ppe_report.xml",
    ],
    "installable": True,
}
