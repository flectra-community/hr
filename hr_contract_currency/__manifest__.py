# Copyright 2018 Brainbean Apps (https://brainbeanapps.com)
# Copyright 2020 CorporateHub (https://corporatehub.eu)
# License AGPL-3.0 or later (https://www.gnu.org/licenses/agpl).

{
    "name": "HR Contract Currency",
    "version": "2.0.1.0.0",
    "category": "Human Resources",
    "website": "https://gitlab.com/flectra-community/hr",
    "author": "CorporateHub, Odoo Community Association (OCA)",
    "license": "AGPL-3",
    "installable": True,
    "summary": "Employee's contract currency",
    "depends": ["hr_contract"],
    "data": ["views/hr_contract.xml"],
}
