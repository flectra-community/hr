# Copyright (C) 2018 Brainbean Apps (https://brainbeanapps.com)
# Copyright 2020 CorporateHub (https://corporatehub.eu)
# License AGPL-3.0 or later (https://www.gnu.org/licenses/agpl.html).

{
    "name": "HR Employee Service",
    "version": "2.0.1.1.1",
    "category": "Human Resources",
    "website": "https://gitlab.com/flectra-community/hr",
    "author": "CorporateHub, Odoo Community Association (OCA)",
    "license": "AGPL-3",
    "installable": True,
    "application": False,
    "summary": "Employee service information & duration",
    "depends": ["hr"],
    "external_dependencies": {"python": ["dateutil"]},
    "data": ["views/hr_employee.xml"],
}
