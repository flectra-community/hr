# Copyright 2021 Ecosoft Co., Ltd. (http://ecosoft.co.th)
# License AGPL-3.0 or later (https://www.gnu.org/licenses/agpl).

{
    "name": "HR department code",
    "version": "2.0.1.0.0",
    "license": "AGPL-3",
    "category": "Human Resources",
    "author": "Ecosoft, Odoo Community Association (OCA)",
    "website": "https://gitlab.com/flectra-community/hr",
    "depends": ["hr"],
    "data": ["views/hr_department_views.xml"],
    "installable": True,
    "maintainer": ["Saran440"],
}
